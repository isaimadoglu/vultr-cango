from django.shortcuts import render, redirect

from .forms import RegisterForm, LoginForm

# user modulunu import edeyim ki, orm sorgularimi yapabileyim.
from django.contrib.auth.models import User

from django.contrib.auth import login, authenticate, logout

from django.contrib import messages

# Create your views here.

# Register fonksiyonu, kayit olma sayfasini cagirir.
def register(request):
    # POST request mi yoksa GET request mi oldugu burada kontrol edilir.
    form = RegisterForm(request.POST or None)
    # forms.py'deki clean metodu dogru calisirsa is_valid, True doner.
    if form.is_valid():
        username = form.cleaned_data.get("username")
        password = form.cleaned_data.get("password")

        newUser = User(username = username)
        newUser.set_password(password)

        newUser.save()
        login(request, newUser)
        messages.success(request, "Başarıyla Kayıt Oldunuz...")
        return redirect("index")
    # forms.is_valid False donerse Form'u Template'e gonderiyorum.
    context = {
            "form" : form
        }
    return render(request, "userapp/register.html", context)

    
def loginUser(request):
    form = LoginForm(request.POST or None)

    context = {
        "form" : form
    }

    if form.is_valid():
        username = form.cleaned_data.get("username")
        password = form.cleaned_data.get("password")

        user = authenticate(username = username, password = password)

        if user is None:
            messages.info(request, "Kullanıcı Adı veya Parola Hatalı.")
            return render(request, "userapp/login.html", context)

        messages.success(request, "Başarıyla Giriş Yaptınız")
        login(request, user)
        return redirect("index")

    return render(request, "userapp/login.html", context)

def logoutUser(request):
    logout(request)
    messages.success(request, "Başarıyla Çıkış Yaptınız")
    return redirect("index")