from django.contrib import admin
from django.urls import path

from . import views

app_name = "userapp"

urlpatterns = [
    # Eger URL adresimize isim verirsem, ilerde redirect islemini ona gore yapabilirim.
    path('register/', views.register, name = "register"),
    path('login/', views.loginUser, name = "login"),
    path('logout/', views.logoutUser, name = "logout"),
]
