from django.contrib import admin
from django.urls import path

from . import views

app_name = "qrcodeapp"

urlpatterns = [
    # Eger URL adresimize isim verirsem, ilerde redirect islemini ona gore yapabilirim.
    path('generate/', views.generateView, name = "generate"),
    path('scan/', views.scanView, name= "scan"),
    #path('', views.articlesViews, name= "articles"),
    #path('article/<int:id>', views.detail, name= "detail")
]
