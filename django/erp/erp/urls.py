"""erp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin

# "include" sayesinde Uygulama Url'i yazabiliyorum.
from django.urls import path, include

# urlpatterns icin
from django.conf import settings
from django.conf.urls.static import static

from article import views

urlpatterns = [
    path('admin/', admin.site.urls),
    # Eger URL adresimize isim verirsem, ilerde redirect islemini ona gore yapabilirim.
    path('', views.index, name = "index"),
    path('about/', views.about, name = "about"),
    path('articles/', include("article.urls")),
    path('user/', include("userapp.urls")),
    path('qrcodeapp/', include("qrcodeapp.urls")),
]

# if settings.DEBUG:
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
