from django.contrib import admin

from .models import Article

# Register your models here.

#admin.site.register(Article)

@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):

    # Django Admin panelinde gosterilecek olan basliklar
    list_display = ["title", "author", "created_date"]

    # title ve created_date'e tiklandiginda Article'in detaylarina gidebiliriz.
    list_display_links = ["title", "created_date"]

    # Sadece "title" bilgisine gore arama ozelligi kazandiririm.
    search_fields = ["title"]

    # "created_date" suzgecine gore bir field olusturuyoruz. Buna gore listeleniyor.
    list_filter = ["created_date"]

    # Buradaki ArticleAdmin class'i ile Article class'ini baglamis oluyoruz.
    # Article modelini ozellestiriyoruz.
    class Meta:
        model = Article