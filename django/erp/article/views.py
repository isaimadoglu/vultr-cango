from django.shortcuts import render, HttpResponse, redirect, get_object_or_404

from django.contrib import messages

# suanki dosyamizdan article form'u almak istiyoruz.
from .forms import ArticleForm

# Veritabanindan Article'lari cekecek sorguyu yapabilmem icin
from .models import Article

# Create your views here.

# request variable must be in every view module as the first parameter.
def index(request):
    return render(request, "article/index.html")

def about(request):
    return render(request, "article/about.html")

def dashboard(request):
    return render(request, "article/dashboard.html")

def articlesViews(request):
    articles = Article.objects.filter(author = request.user)
    context = {
        "articles": articles
    }
    return render(request, "article/articles.html", context)

def addArticle(request):
    form = ArticleForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        # article'in author bilgisini vermek icin commit=False yapariz.
        article = form.save(commit=False)
        article.author = request.user
        article.save()

        messages.success(request, "Article has been created successfully.")
        return redirect("index")

    return render(request, "article/addarticle.html", {"form":form})

# kullanimi: http://localhost:8000/detail/232
def detail(request,id):
    # filter(id=id) yaptigimiz zaman bize QuerySet seklinde bir liste donuyor.
    # article = Article.objects.filter(id = id).first()
    article = get_object_or_404(Article, id = id)
    return render(request, "article/detail.html", {"article": article})

def upload(request):
    if request.method == 'POST':
        uploaded_file = request.FILES['document']
        print(uploaded_file.name)
        print(uploaded_file.size)
    return render(request, 'article/upload.html')