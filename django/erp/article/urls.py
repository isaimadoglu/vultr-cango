from django.contrib import admin
from django.urls import path

from . import views

# urlpatterns icin
from django.conf import settings
from django.conf.urls.static import static

app_name = "article"

urlpatterns = [
    # Eger URL adresimize isim verirsem, ilerde redirect islemini ona gore yapabilirim.
    path('dashboard/', views.dashboard, name = "dashboard"),
    path('addarticle/', views.addArticle, name= "addarticle"),
    path('', views.articlesViews, name= "articles"),
    path('article/<int:id>', views.detail, name= "detail"),
    path('upload/', views.upload, name= "upload"),
]

# Biz bu yapiyi almazsak eger biz media url'mize ve media root'umuza, python dosyalarindan erisemicez.
# if settings.DEBUG:
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
